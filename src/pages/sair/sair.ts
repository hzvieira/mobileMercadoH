import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';

/*
  Generated class for the Sair page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-sair',
  templateUrl: 'sair.html'
})
export class SairPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
  public platform: Platform) {
    localStorage.setItem('token', null);
    localStorage.setItem('cliente_id', null);
    this.platform.exitApp()
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SairPage');
  }

}
