import { ClienteService } from './../cliente.service';
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

/*
  Generated class for the ClienteDadosPessoais page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-cliente-dados-pessoais',
  templateUrl: 'cliente-dados-pessoais.html'
})
export class ClienteDadosPessoaisPage {
  dadosPessoais: any = new Object()

  constructor(public navCtrl: NavController, public navParams: NavParams,
  public viewCtrl: ViewController, public _clienteService: ClienteService) {
    console.log('Dados: ', navParams.get('dadosPessoais'))
    let objeto = navParams.get('dadosPessoais')
    if (objeto) {
      this.dadosPessoais = objeto
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClienteDadosPessoaisPage');
  }

  abrirBotao(): boolean {
    let abrirBotao = true
    if (this.dadosPessoais.nome == '' || !this.dadosPessoais.nome) {
      abrirBotao = false
    } else if (this.dadosPessoais.telefone == '' || !this.dadosPessoais.telefone) {
      abrirBotao = false
    } else if (this.dadosPessoais.cpf == '' || !this.dadosPessoais.cpf) {
      abrirBotao = false
    } 
    return abrirBotao
  }

  adicionar() {
    this._clienteService.setDadosPessoais(this.dadosPessoais).subscribe(
       data => this.sair(data),
       error => console.log('Erro ao alterar dados: ', error)
    )
    this.sair(this.dadosPessoais)
  }

  sair(dadosPessoais){
    this.viewCtrl.dismiss(dadosPessoais)
  }

  voltarPage() {
    this.navCtrl.pop()//this.viewCtrl.dismiss();
  }

}
