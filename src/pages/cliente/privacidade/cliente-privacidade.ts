import { ClienteService } from './../cliente.service';
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController } from 'ionic-angular';

/*
  Generated class for the ClientePrivacidade page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-cliente-privacidade',
  templateUrl: 'cliente-privacidade.html'
})
export class ClientePrivacidadePage {
  dadosPrivacidade: any = new Object()
  cliente: any = new Object()

  constructor(public navCtrl: NavController, public navParams: NavParams,
  public viewCtrl: ViewController, public alertCtrl: AlertController, 
  public _clienteService: ClienteService) {
    let objeto = navParams.get('dadosPrivacidade')
    if (objeto) {
      this.cliente = objeto
      this.dadosPrivacidade.cliente_id = objeto.cliente_id
      this.dadosPrivacidade.email = objeto.email
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClientePrivacidadePage');
  }

  abrirBotao(): boolean {
    let abrirBotao = true
    if (this.dadosPrivacidade.email == '' || !this.dadosPrivacidade.email) {
      abrirBotao = false
    } else if (this.dadosPrivacidade.senhaAtual == '' || !this.dadosPrivacidade.senhaAtual) {
      abrirBotao = false
    } else if (this.dadosPrivacidade.novaSenha == '' || !this.dadosPrivacidade.novaSenha) {
      abrirBotao = false
    } else if (this.dadosPrivacidade.confirmacaoNovaSenha == '' || !this.dadosPrivacidade.confirmacaoNovaSenha) {
      abrirBotao = false
    } 
    return abrirBotao
  }

  adicionar() {
    if (this.cliente.senha != this.dadosPrivacidade.senhaAtual) {
      this.criarAlert(
        'Senha atual incorreta', 'A senha atual está incorreta'
      )
    } else if (this.dadosPrivacidade.novaSenha != this.dadosPrivacidade.confirmacaoNovaSenha) {
      this.criarAlert(
        'Senhas incorretas', 'A confirmação de senha não está batendo, tem alguma coisa errada aew! :D'
      )
    } else {
      this._clienteService.setDadosPrivacidade(this.dadosPrivacidade).subscribe(
        data => this.sair(),
        error => console.log('Erro ao alterar dados: ', error)
      )
     
    }
  }

  criarAlert(titulo, subTitulo) {
    let alert = this.alertCtrl.create({
      title: titulo,
      subTitle: subTitulo,
      buttons: ['Ok']
    })    
    alert.present();
  }

  sair(){
    this.viewCtrl.dismiss(true)
  }
   
  voltarPage() {
    this.navCtrl.pop()//this.viewCtrl.dismiss();
  }

}

