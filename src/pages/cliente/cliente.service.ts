import { Http } from '@angular/http';
import { HttpService } from './../../app/app.interception.service';
import { Injectable } from '@angular/core';

import { SERVER } from './../../app/app.constantes'

@Injectable()
export class ClienteService {

  constructor(private _http: HttpService, private http: Http) { }

  postEndereco(endereco) {
    return this._http.post(SERVER.endereco + 'aplicativo/cliente/endereco/', endereco)
      .map(res=>res.json())
  }

  isLogado() {
    return this._http.request(SERVER.endereco + 'aplicativo/isLogado')
      .map(res=>res.json())
  }

  getInformacoesCliente(idCliente) {
    return this._http.request(SERVER.endereco + 'aplicativo/cliente/informacoes/' + idCliente)
      .map(res=>res.json())
  }

  novoCadastro(cliente) {
    return this.http.post(SERVER.endereco + 'aplicativo/cliente/cadastro/novo', cliente)
      .map(res=>res.json())
  }

  setDadosPessoais(dadosPessoais) {
    return this._http.put(SERVER.endereco + 'aplicativo/cliente/setDadosPessoais', dadosPessoais)
      .map(res=>res.json())
  }

  setDadosPrivacidade(dadosPrivacidade) {
    return this._http.put(SERVER.endereco + 'aplicativo/cliente/setDadosPrivacidade', dadosPrivacidade)
      .map(res=>res.json())
  }

  setEnderecoFalse(endereco) {
    return this._http.put(SERVER.endereco + 'aplicativo/cliente/setEnderecoFalse', endereco)
      .map(res=>res.json())
  }

  getLojaIdWithAdrres(endereco) {
    return this._http.request(SERVER.endereco + 'aplicativo/loja/getId/endereco/' + endereco.cidade_id)
      .map(res=>res.json())
  }

}