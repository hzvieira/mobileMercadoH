import { ClienteService } from './../cliente.service';
import { LocalizacaoService } from './../../../util/localizacao/localizacao.service';
import { Geolocation } from 'ionic-native';

import { Component } from '@angular/core';
import { NavController, AlertController, ViewController, LoadingController } from 'ionic-angular';

/*
  Generated class for the CadastroEndereco page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-cadastro-endereco',
  templateUrl: 'cadastro-endereco.html'
})
export class CadastroEnderecoPage {
  // Loading
  protected loading: any

  protected cidades: Object[]

  endereco: any = new Object({localizacao: false})

  constructor(private _localizacaoService: LocalizacaoService,
              private navCtrl: NavController,
              private _clienteService: ClienteService,
              public alertCtrl: AlertController,
              public load: LoadingController,
              public viewCtrl: ViewController) {
     this.getCidades()
  }

  criarLoading (mensagem) {
    this.loading = this.load.create({
       content: mensagem ? mensagem : 'Por favor, aguarde enquanto as informações são carregadas!'
    })
    this.loading.present();
  }

  fecharLoading() {
    this.loading.dismiss()
  }

  getCidades() {
    this._localizacaoService.getCidades().subscribe(
      data => this.cidades = data,
      error => console.log('Erro ao buscar cidades!')
    )
  }

  adicionar() {
    if (this.endereco.localizacao) {
      this.salvarEndereco(null)
    } else {
      this.criarLoading(null)
      let enderecoBuscaMaps = `${this.endereco.rua}, ${this.endereco.numero} - ${this.endereco.bairro}, ${this.getNomeCidade()}, ${this.endereco.cep}` 
      this._localizacaoService.getEnderecoMapsAddress(enderecoBuscaMaps).subscribe(
        data => this.tratarBuscaMaps(data),
        error => console.log('Erro ao buscar cidades!')
      )
    }
  }

  salvarEndereco(cidadePesquisada) {
    this.endereco.cliente_id = localStorage.getItem('cliente_id')
    this._clienteService.postEndereco(this.endereco).subscribe(
      data => {
        data.nomeCidade = cidadePesquisada.nome
        data.nomeEstado = cidadePesquisada.estado
        this.sair(data)},
      error => console.log('Erro ao cadastrar endereco')
    )  
  }

  sair(endereco) {
    this.viewCtrl.dismiss(endereco);
  }

  digitarEndereco() {
    this.endereco.localizacao = false // = new Object({localizacao: false})
  }

  abrirBotao(): boolean {
    let abrirBotao = true
    if (this.endereco.rua == '' || !this.endereco.rua) {
      abrirBotao = false
    } else if (this.endereco.bairro == '' || !this.endereco.bairro) {
      abrirBotao = false
    } else if (this.endereco.numero == '' || !this.endereco.numero) {
      abrirBotao = false
    } else if (this.endereco.cep == '' || !this.endereco.cep) {
      abrirBotao = false
    } else if (this.endereco.cidade_id < 0 || !this.endereco.cidade_id) {
      abrirBotao = false
    }
    return abrirBotao
  }

  getMinhaLocalizacao() {
    this.criarLoading(null)
    this.endereco = new Object({localizacao: true})
    Geolocation.getCurrentPosition().then(position => {
      this.getDadosPosition(position.coords)
    }, (err) => {
      console.log(err);
    });
  }

  getDadosPosition(coords) {
    this._localizacaoService.getEnderecoMaps(coords).subscribe(
      data => this.filtrarDadosMaps(data),
      error => console.log('Error: ', error)
    )
  }

  getCidadeId(cidade) {
    this._localizacaoService.getCidadeId(cidade).subscribe(
      data => this.tratarCidade(data, cidade),
      error => console.log('Error: ', error)
    )
  }

  getNomeCidade() {
    let nomeCidade
    this.cidades.forEach(objeto => {
      let cidade: any = objeto
      if (cidade.id == this.endereco.cidade_id) {
        nomeCidade = cidade.nome + '- ' + cidade.estado
        return nomeCidade
      }
    })
    return nomeCidade
  }

  tratarCidade(data, cidadePesquisada) {
    // let id = data[0].id
    this.fecharLoading()
    console.log('Endereco: ', this.endereco)
    if (data[0]) {
      this.endereco.cidade_id = data[0].id
      if (!this.endereco.localizacao) {
        let alert = this.alertCtrl.create({
          title: 'Endereço encontrado!!',
          subTitle: `O endereço encontrado é: 
             ${this.endereco.rua}, n° ${this.endereco.numero} - ${this.endereco.bairro}, 
             ${cidadePesquisada.nome} (${cidadePesquisada.estado}), Cep: ${this.endereco.cep}.
             Confirma informações e salva o endereço?`,
          buttons: [
            {
              text: 'Não',
              role: 'cancel',
              handler: () => {}
            },
            {
              text: 'Sim',
              handler: () => {
                // this.endereco.nomeCidade = cidadePesquisada.nome
                // this.endereco.nomeEstado = cidadePesquisada.estado
                this.salvarEndereco(cidadePesquisada)
              }
            }
          ]
        })    
        alert.present();
      }
    } else {
      let alert = this.alertCtrl.create({
        title: 'Não atendemos essa cidade',
        subTitle: 'Estamos trabalhando arduamente para chegar em todas as cidades, mas ainda não chegamos em ' + 
        cidadePesquisada.nome + '(' + cidadePesquisada.estado + ')',
        buttons: ['Fazer o que né ¯\\_(ツ)_/¯']
      })    
      alert.present();
    }
  }

  tratarBuscaMaps(data) {
    console.log('Data: ', data)
    if (data.status == "ZERO_RESULTS") {
      this.fecharLoading()
      let alert = this.alertCtrl.create({
        title: 'Endereço não encontrado',
        subTitle: 'O endereço digitado não foi encontrado, por favor, verifique se os dados estão digitados corretamente!!',
        buttons: ['Ok']
      })    
    alert.present();
    } else {
      this.filtrarDadosMaps(data)
    }
  }

  filtrarDadosMaps(data: any) {
    let dados = data.results[0].address_components
    let cidade: any = new Object()

    dados.forEach(element => {
      element.types.forEach(tipo => {
        if (tipo == 'street_number') {
          this.endereco.numero = element.long_name
        }
        if (tipo == 'route') {
          this.endereco.rua = element.long_name
        }
        if (tipo == 'sublocality') {
          this.endereco.bairro = element.long_name
        }
        if (tipo == 'administrative_area_level_2') {
          cidade.nome = element.long_name
        }
        if (tipo == 'administrative_area_level_1') {
          cidade.estado = element.short_name
        }
        if (tipo == 'postal_code') {
          this.endereco.cep = element.short_name
        }
      })
    });
    this.getCidadeId(cidade)
  }

  isLocalizacao(): boolean {
    return this.endereco.localizacao
  }

  voltarPage() {
    this.navCtrl.pop()//this.viewCtrl.dismiss();
  }

}
