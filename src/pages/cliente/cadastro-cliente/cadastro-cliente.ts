import { ClienteService } from './../cliente.service';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';

/*
  Generated class for the CadastroCliente page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-cadastro-cliente',
  templateUrl: 'cadastro-cliente.html'
})
export class CadastroClientePage {
  cliente: any = new Object()

  constructor(public navCtrl: NavController, public navParams: NavParams,
  public alertCtrl: AlertController, public _clienteService: ClienteService) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad CadastroClientePage');
  }

  abrirBotao(): boolean {
    let abrirBotao = true
    if (this.cliente.nome == '' || !this.cliente.nome) {
      abrirBotao = false
    } else if (this.cliente.telefone == '' || !this.cliente.telefone) {
      abrirBotao = false
    } else if (this.cliente.cpf == '' || !this.cliente.cpf) {
      abrirBotao = false
    } else if (this.cliente.email == '' || !this.cliente.email) {
      abrirBotao = false
    } else if (this.cliente.senha == '' || !this.cliente.senha) {
      abrirBotao = false
    } else if (this.cliente.senhaDeNovo == '' || !this.cliente.senhaDeNovo) {
      abrirBotao = false
    } 
    return abrirBotao
  }
  
  voltarPage() {
    this.navCtrl.pop()//this.viewCtrl.dismiss();
  }

  adicionar() {
    if (this.cliente.senha != this.cliente.senhaDeNovo) {
      this.criarAlert(
        'Senha atual incorreta', 'A senha atual está incorreta'
      )
      this.cliente.senhaDeNovo = ''
    } else {
      this._clienteService.novoCadastro(this.cliente).subscribe(
        data => this.sair(),
        error => this.tratarErro(error) //console.log('Erro ao criar usuário dados: ', error)
      )
     
    }
  }

  tratarErro(erro) {
    let erroCorpo = JSON.parse(erro._body)
    if (erroCorpo.constraint == "cliente_email_unique") {
      this.criarAlert(
        'Erro ao cadastrar', 'Isso é embaraçoso, mas já temos esse email em nosso sistema, poderia digitar outro?'
      )
      this.cliente.email = ''
    } else if (erroCorpo.constraint == "cliente_cpf_unique") {
      this.criarAlert(
        'Erro ao cadastrar', 'Isso é embaraçoso, mas já temos esse cpf em nosso sistema, poderia digitar outro?'
      )
      this.cliente.cpf = ''
    } else if (erroCorpo.constraint == "cliente_telefone_unique") {
      this.criarAlert(
        'Erro ao cadastrar', 'Isso é embaraçoso, mas já temos esse telefone em nosso sistema, poderia digitar outro?'
      )
      this.cliente.telefone = ''
    }
    console.log('Erro ao criar usuário dados: ', )
  }

  criarAlert(titulo, subTitulo) {
    let alert = this.alertCtrl.create({
      title: titulo,
      subTitle: subTitulo,
      buttons: ['Ok']
    })    
    alert.present();
  }

  sair(){
    this.criarAlert(
        'Aewwww', 'Já pode fazer as compras de boas!!'
      )
    this.navCtrl.pop()
  }
}
