import { ClientePrivacidadePage } from './../cliente/privacidade/cliente-privacidade';
import { ClienteDadosPessoaisPage } from './../cliente/dados-pessoais/cliente-dados-pessoais';
import { CadastroEnderecoPage } from './../cliente/cadastro-endereco/cadastro-endereco';
import { ClienteService } from './../cliente/cliente.service';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ModalController, Content, AlertController, LoadingController } from 'ionic-angular';

/*
  Generated class for the Settings page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {
  @ViewChild(Content) content: Content;
  // Loading
  protected loading: any
  protected cliente: any

  protected mostrarDadosPessoais = false
  protected mostrarDadosPrivacidade = false
  protected mostrarDadosEndereco = false
  

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public modalCtrl: ModalController,
              public alertCtrl: AlertController,
              public _clienteService: ClienteService,
              public load: LoadingController) {
    this.cliente = new Object()
    this.criarLoading(null)
    this.getInformacoesCliente()
  }

  criarLoading (mensagem) {
    this.loading = this.load.create({
       content: mensagem ? mensagem : 'Por favor, aguarde enquanto as informações são carregadas!'
    })
    this.loading.present();
  }

  fecharLoading() {
    this.loading.dismiss()
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }

  getInformacoesCliente() {
    let cliente_id = localStorage.getItem('cliente_id');
    this._clienteService.getInformacoesCliente(cliente_id).subscribe(
      data => this.cliente = data,
      error => console.log('Error: ', JSON.stringify(error, null, 2)),
      () => this.fecharLoading()
    )
  }

  selecionarEndereco(endereco) {
    console.log('selecionarEndereco');
    let alert = this.alertCtrl.create({
      title: 'Excluir endereço',
      message: 'Você deseja mesmo excluir o endereço selecionado??',
      buttons: [
        {
          text: 'Não',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Sim',
          handler: () => {
            this.excluirEndereço(endereco)
          }
        }
      ]
    })    
    alert.present();
  }

  excluirEndereço(endereco) {
    console.log('excluirEndereço');
    this._clienteService.setEnderecoFalse(endereco).subscribe(
      data => this.navCtrl.setRoot(this.navCtrl.getActive().component),
      error => console.log('Error: ', JSON.stringify(error, null, 2)),
      // () => console.log('Cliente', JSON.stringify(this.cliente, null, 2))
    )
  }

  adicionarEndereco(endereco) {
    let profileModal = this.modalCtrl.create(CadastroEnderecoPage);
    profileModal.onDidDismiss(endereco => {
      console.log('Dentro do dismis modal: ', endereco);
      if (endereco) {
        this.navCtrl.setRoot(this.navCtrl.getActive().component);
      }
    });
    profileModal.present();
  }

  alterarDadosPessoais() {
    let dadosPessoais = {
      cliente_id: this.cliente.id,
      nome: this.cliente.nome,
      telefone: this.cliente.telefone,
      cpf: this.cliente.cpf
    }
    console.log('alterarDadosPessoais');
    let profileModal = this.modalCtrl.create(ClienteDadosPessoaisPage, {dadosPessoais: dadosPessoais});
    profileModal.onDidDismiss(objeto => {
      console.log('Dentro do dismis modal: ', objeto);
      if (objeto) {
        this.navCtrl.setRoot(this.navCtrl.getActive().component);
      }
    });
    profileModal.present();
  }

  alterarDadosPrivacidade() {
    console.log('alterarDadosPrivacidade');
    let dadosPrivacidade = {
      cliente_id: this.cliente.id,
      senha: this.cliente.senha,
      email: this.cliente.email,
    }
    let profileModal = this.modalCtrl.create(ClientePrivacidadePage, {dadosPrivacidade: dadosPrivacidade});
    profileModal.onDidDismiss(objeto => {
      console.log('Dentro do dismis modal: ', objeto);
      if (objeto) {
        this.navCtrl.setRoot(this.navCtrl.getActive().component);
      }
    });
    profileModal.present();
  }

}
