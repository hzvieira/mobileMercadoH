import { CadastroClientePage } from './../cliente/cadastro-cliente/cadastro-cliente';
import { LoginService } from './login.service';
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ModalController, AlertController } from 'ionic-angular';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  usuario: any = new Object()

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public loginService: LoginService,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController) {}

  ionViewDidLoad() {

  }

  loggar() {
    this.loginService.fazerLogin(this.usuario).subscribe(
      data => this.concluirLoggin(data),
      error => this.naoLogou(error)
    )
  }

  concluirLoggin(data) {
    console.log('Login: ', data)
    localStorage.setItem('token', data.token);
    localStorage.setItem('cliente_id', data.id);
    this.irAsCompras()
  }

  naoLogou(error) {
    // alert('NOPS: ' + error)
    this.alertNaoLogou()
  }

  alertNaoLogou() {
    let alert = this.alertCtrl.create({
      title: 'Não encontrado!!',
      message: 'Não achamos as informaçoẽs em nosso sistema, verifique se os dados foram digitados corretamente!!',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            // console.log('Cancel clicked');
          }
        },
        // {
        //   text: 'Sim',
        //   handler: () => {
        //     // console.log('Verificar loja')
        //     this.carrinhoDeCompra = dados
        //   }
        // }
      ]
    })    
    alert.present();
  }

  irAsCompras() {
    //this.navCtrl.setRoot(InicioPaginaPage)
     window.location.reload()
     this.viewCtrl.dismiss();
  }

  criarCadastro() {
    let profileModal = this.modalCtrl.create(CadastroClientePage);
    profileModal.present()
  }

}
