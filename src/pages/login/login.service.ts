import { SERVER } from './../../app/app.constantes';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class LoginService {

  constructor(private _http: Http) { }

  fazerLogin(usuario: any){
    return this._http.post(SERVER.endereco + 'logar/aplicativo', usuario)
         .map(res => res.json());
  }
}


  // if (usuario.nome === 'usuario@email.com' && 
  //     usuario.senha === '123456') {
  //     this.mostrarMenuEmitter.emit(true);
  //     this.router.navigate(['/']);
  //   } else {
  //     this.mostrarMenuEmitter.emit(false);
  //   }