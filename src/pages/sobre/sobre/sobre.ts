import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TermoDeUsoPage } from "../termo-de-uso/termo-de-uso";
import { PoliticaPrivacidadePage } from "../politica-de-privacidade/politica-privacidade";

@Component({
  selector: 'page-about',
  templateUrl: 'sobre.html'
})
export class SobrePage implements OnInit {


    constructor(public navCtrl: NavController) {}

    ngOnInit(): void {
        
    }

    irTermoDeUso() {
        this.navCtrl.push(TermoDeUsoPage);
    }

    irPoliticaPrivacidade() {
        this.navCtrl.push(PoliticaPrivacidadePage);
    }

}