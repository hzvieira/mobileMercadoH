import { CompraPage } from './../comprar/compra/compra';
import { ComprarService } from './../comprar/comprar.service';
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';

/*
  Generated class for the ComprasRealizadas page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-compras-realizadas',
  templateUrl: 'compras-realizadas.html'
})
export class ComprasRealizadasPage {
  // Loading
  protected loading: any

  protected comprasRealizadas: Object[]

  constructor(public navCtrl: NavController, 
          public navParams: NavParams,
          public compraService: ComprarService,
          public load: LoadingController,) {
            this.getComprasRealizadas()
          }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ComprasRealizadasPage');
  }

  getComprasRealizadas() {
    let cliente_id = localStorage.getItem('cliente_id');
    this.criarLoading(null)
    this.compraService.getComprasRealizadas(cliente_id).subscribe(
      data => this.comprasRealizadas = data,
      error => alert(error._body),
      () => this.fecharLoading()
    )
  }

  criarLoading (mensagem) {
    this.loading = this.load.create({
       content: mensagem ? mensagem : 'Por favor, aguarde enquanto as informações são carregadas!'
    })
    this.loading.present();
  }

  fecharLoading() {
    this.loading.dismiss()
  }

  corCard(situacao) {
    if (situacao == 'compra') {
      return 'secondary'
    } else if (situacao == 'montagem') {
      return 'royal'
    } else if (situacao == 'entregue') {
      return 'primary'
    } else if (situacao == 'cancelado') {
      return 'danger'
    }
  }

  iconeCard(situacao) {
    if (situacao == 'compra') {
      return 'cart'
    } else if (situacao == 'montagem') {
      return 'apps'
    } else if (situacao == 'entregue') {
      return 'cube'
    } else if (situacao == 'cancelado') {
      return 'danger'
    }
  }

  getTotalCompra(itens) {
    var total = 0
    for (let item of itens) {
      total += item['preco'] * item['quantidade']
    }
    return total.toFixed(2)
  }

  getTotalCompraComFrete(itens, frete) {
    var total = frete
    for (let item of itens) {
      total += item['preco'] * item['quantidade']
    }
    return total.toFixed(2)
  }

  getQuantidadeItens(itens) {
    var total = 0
    for (let item of itens) {
      total += item['quantidade']
    }
    return total
  }

    // Acordion da lista
  public showCompra: any
  toggleDetails(id) {
    if (this.isShowCompra(id)) {
      this.showCompra = null 
    } else {
      this.showCompra = id
    }
  }

  isShowCompra(id: any) {
    return this.showCompra === id
  }

  fazerCompra() {
    this.navCtrl.setRoot(CompraPage);
  }

}
