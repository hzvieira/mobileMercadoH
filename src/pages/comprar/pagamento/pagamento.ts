import { ClienteService } from './../../cliente/cliente.service';
import { PopoverCarrinho } from './popoverCarrinho/popover';
import { FinalizacaoPage } from './../finalizacao/finalizacao';
import { Storage } from '@ionic/storage';
import { ComprarService } from './../comprar.service';
import { Component, ViewChild, OnInit } from '@angular/core';
import { NavController, NavParams, Nav, LoadingController, PopoverController, AlertController } from 'ionic-angular';

/*
  Generated class for the Pagamento page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-pagamento',
  templateUrl: 'pagamento.html'
})
export class PagamentoPage implements OnInit{
  // Loading
  protected loading: any

  @ViewChild(Nav) nav: Nav;

  protected loja: any = new Object()
  protected carrinhoDeCompra: Array<any> = new Array<Object>()
  protected entrega: any

  protected formaDePagamento: string
  protected itemPagamento: any
  protected totalItens: any
  protected freteLoja: any
  protected totalCompra: any

  protected botoesDinheiro: Array<any> = new Array<Object>()

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public _comprarService: ComprarService,
              public _clienteService: ClienteService,
              public storage: Storage,
              public load: LoadingController,
              public alertCtrl: AlertController,
              public popoverCtrl: PopoverController
              ) {
  }

  ionViewDidLoad() { }

  ngOnInit() {
      this.criarLoading(null)
      this.storage.get(`carrinhoDeCompra`)
        .then((carrinho) => {this.carrinhoDeCompra = carrinho, this.getInformacoesLoja()} //this.atribuirCarrinho(carrinho)
      )
      this.entrega = this.navParams.get('entrega')
  }

  // qualLoja() {
  //   let endereco = this.navParams.get('endereco')
  //   this._clienteService.getLojaIdWithAdrres(endereco).subscribe(
  //     data => this.tratarLoja(data[0].id),//localStorage.setItem('loja_id', data[0].id),
  //     error => console.log('Error: ', JSON.stringify(error, null, 2))
  //   )
  // }

  // tratarLoja(loja_id) {
  //   let loja_id_aplicativo = localStorage.getItem('loja_id')
  //   if (loja_id == loja_id_aplicativo) {
  //     // console.log('Igual')
  //     this.getInformacoesLoja(loja_id)
  //   } else {
  //     console.log('Avisar o usuário')
  //     let arrayIds: Array<any> = new Array<Object>()
  //     for (let item of this.carrinhoDeCompra) {
  //       arrayIds.push(item['item'].id)
  //     }

  //     this._comprarService.getItensLojaAtualizada(loja_id, arrayIds).subscribe(
  //       data =>  console.log('ITens: ', JSON.stringify(data, null, 2)),//localStorage.setItem('loja_id', data[0].id),
  //       error => console.log('Error d: ', JSON.stringify(error, null, 2))
  //     )

  //     console.log('Buscar preços e informaçoes: ', arrayIds)
  //   }

  // }

  criarLoading (mensagem) {
    this.loading = this.load.create({
       content: mensagem ? mensagem : 'Por favor, aguarde enquanto as informações são carregadas!'
    })
    this.loading.present();
  }

  fecharLoading() {
    this.loading.dismiss()
  }

  whoFormaDePagamento(formaDePagamento):boolean {
    if (this.formaDePagamento == formaDePagamento) {
      return true
    } 
    return false
  }

  presentPopover(myEvent) {
    console.log('Clique item')
    let popover = this.popoverCtrl.create(PopoverCarrinho);
    popover.present({
      ev: myEvent
    });
  }

  selecionarFormaDePagamento(formaDePagamento) {
    this.formaDePagamento = formaDePagamento
  }

  getInformacoesLoja() {
    let loja_id = localStorage.getItem('loja_id');
    this._comprarService.getInformacoesLoja(loja_id).subscribe(
      data => this.atribuirLoja(data),
      error => console.log('Error: ', JSON.stringify(error, null, 2))
    )
  }

  atribuirLoja(loja) {
    this.loja = loja
    this.fazerCalculoFrete()
  }

  fazerCalculoFrete() {
    if (this.loja.configuracaoEntrega.custoFixo) {
      this.freteLoja = this.loja.taxaDeEntrega.custoFixo.toFixed(2)
    } else {
      this.freteLoja = 15.00
    }
    this.fazerCaluculoCompra()
  }

  fazerCaluculoCompra() {
    var total = 0
    for (let item of this.carrinhoDeCompra) {
      total += item['item'].preco * item['quantidade']
    }
    this.totalItens = total.toFixed(2)
    this.fazerCalculoTotalCompra()
  }

  fazerCalculoTotalCompra() {
    this.totalCompra = (+this.totalItens + +this.freteLoja).toFixed(2)
    this.gerarBotoesPagamento()     
  }

  gerarBotoesPagamento() {
    let valorCompra = +this.totalCompra

		let botoes = []
		let compraMaisCentavos = 0
  	let terceiraNota = 0
  	let ultimaNota = 0

		botoes.push(valorCompra)

    let centavos = (+valorCompra % 10)
  	if (centavos < 5 && centavos > 0) {
    	compraMaisCentavos = (5 - centavos) + valorCompra
      botoes.push(compraMaisCentavos)
  	} else if (centavos == 5) {
    	compraMaisCentavos = valorCompra + 5
      botoes.push(compraMaisCentavos)
  	} else if (centavos > 5) {
    	compraMaisCentavos = (10 - centavos) + valorCompra
      botoes.push(compraMaisCentavos)
  	}
  
  	// Terceiro botão
  	let divisao = (valorCompra / 10).toFixed(0)
  	terceiraNota = (+divisao * 10) + 10
  	if (terceiraNota % 50 != 30 || compraMaisCentavos % 25 == 0) {
    	botoes.push(terceiraNota)
  	}

  	// Último botão
  	let troco50 = (valorCompra % 50)
  	let modCompra = ((valorCompra - troco50) / 50).toFixed(0)
    let modCompraNumber = +modCompra
  	if (terceiraNota % 50 == 0) {
   	  ++modCompraNumber
  	}
  	ultimaNota = ((++modCompraNumber) * 50)
    if (compraMaisCentavos % 50 != 0 && terceiraNota % 100 != 0) {
      botoes.push(ultimaNota)
    } else if (valorCompra % 10 == 0) {
      botoes.push(ultimaNota)
    }

  	// Mais uma
  	if (ultimaNota % 50 == 0 && botoes.length == 3 && terceiraNota % 100 != 0) {
    	botoes.push(ultimaNota + 50)    
  	}

    this.botoesDinheiro = botoes
    this.fecharLoading()
  }

  selecionarValorPagamento(objeto) {
    this.itemPagamento = objeto
  }

  finalizarCompra() {
    if (!this.itemPagamento) {
      // console.log('Escolha o turno')
      let alert = this.alertCtrl.create({
        title: 'Forma de pagamento',
        subTitle: 'Para facilitar e agilizar a entrega, por favor, selecione a forma de pagamento!!',
        buttons: ['Ok']
      })    
      alert.present();
    } else {
      let cliente_id = localStorage.getItem('cliente_id');
      let objeto = {
        nota: {formaDePagamento: this.formaDePagamento, itemPagamento: this.itemPagamento, itens: this.carrinhoDeCompra, data: new Date(), loja_id: this.loja.id, cliente_id: cliente_id },
        entrega: {turnoEntrega_id: this.entrega.turnoEntrega, taxaDeEntrega: this.freteLoja, enderecoEntrega_id: this.entrega.enderecoEntrega}
      }

      this._comprarService.postCompra(objeto).subscribe(
          data => this.concluirCompra(data),
          error => console.log('Error: ', error),
          () => console.log('Final: ')
      )
      // console.log('Finalizar compra: ', JSON.stringify(objeto, null, 2))
    }
  }

  concluirCompra(nota) {
    this.storage.remove(`carrinhoDeCompra`)
    this.navCtrl.setRoot(FinalizacaoPage);
  }

  getTotalItens() {
    var itens = 0
    for (let item of this.carrinhoDeCompra) {
      itens += item['quantidade']
    }
    return itens
  }

  temTroco() {
    if (this.itemPagamento > 0 && this.formaDePagamento == 'Dinheiro') {
      return true
    }
    return false
  }

  bandeiraCartao(cartao) {
    if (this.itemPagamento != cartao) {
      return 'filter-grayscale'
    }
  }
}
