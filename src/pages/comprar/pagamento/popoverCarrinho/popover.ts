import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams, ViewController } from 'ionic-angular';

/*
  Generated class for the ComprasRealizadas page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'popover-carrinho',
  templateUrl: 'popover.html'
})
export class PopoverCarrinho {
  protected carrinhoDeCompra: Array<any> = new Array<Object>()

  constructor(public navCtrl: NavController, 
          public navParams: NavParams,
          public viewCtrl: ViewController,
          public storage: Storage) {

    this.storage.get(`carrinhoDeCompra`)
       .then((carrinho) => this.carrinhoDeCompra = carrinho
    )          
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PopoverCarrinho');
  }

  // fecharCard() {
  //   this.viewCtrl.dismiss();
  // }
}
