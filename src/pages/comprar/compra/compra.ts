import { InicioPaginaPage } from './../inicioPagina/inicioPagina';
import { EntregaPage } from './../entrega/entrega';
import { ComprarService } from './../comprar.service';
import { Storage } from '@ionic/storage'

import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, NavParams, LoadingController, Slides, AlertController } from 'ionic-angular';

@Component({
  selector: 'page-compra',
  templateUrl: 'compra.html'
})
export class CompraPage implements OnInit {
  public tabName: any
  private loja_id: any
  // Áreas para listagem
  protected areas: Object[]
  // Chaves de uma subArea
  protected keysObjetos: Array<Object>
  // Objetos de uma subArea
  protected objetosProduto: Array<Object>
  // Índice de apresentação da subArea no card
  protected indexProduto: Array<Object>
  // Card que ira ser mostrado
  protected card: any
  protected cardCarrinho: any
  // Carrinho de compra
  protected carrinhoDeCompra: Array<any>

  // Loading
  protected loading: any
  @ViewChild('mySlider') slider: Slides; 

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public compraService: ComprarService,
    public load: LoadingController,
    public alertCtrl: AlertController,
    public storage: Storage,
    ) {
      this.objetosProduto = new Array<Object>()
      this.keysObjetos = new Array<Object>()
      this.indexProduto = new Array<Object>()
      this.carrinhoDeCompra = new Array<Object>()
      this.tabName = 'lista'

      console.log('Construtor compra')
    }

  ionViewDidLoad() {
   
    // LocalizacaoUsuario.lojaParaPesquisarPreco.subscribe(
    //   loja => this.setarLoja(loja),
    // )
    this.storage.get(`carrinhoDeCompra`)
      .then(
        (valor) => {if (valor) this.criarAlertTemItensCarrinho(valor)}
      )
  }

  ngOnInit() {
    this.loja_id = localStorage.getItem('loja_id')
    if (this.loja_id == null) {
      this.navCtrl.setRoot(InicioPaginaPage)
    }

    console.log('Loja ID: ', this.loja_id)
    this.getAreas()  
	}

  getAreas() {
    this.criarLoading(null)
    this.compraService.getAreas().subscribe(
      data => this.areas = data,
      error => alert(error._body),
      () => this.fecharLoading()
    )
  }

  adicionarCard(produto) {
    this.criarLoading(null)
    var nomeProduto = produto.nome
    if (!this.objetosProduto[nomeProduto]) {
      this.indexProduto[nomeProduto] = produto.apresentacao
      this.compraService.getItens(produto.id, this.loja_id).subscribe(
        data => {this.preencheObjetosProduto(data, nomeProduto)},
        error => alert('Fu'),
        () => {this.finalizaAberturaDoCard(produto)} 
      )
    } else {
      this.criarCard(this.objetosProduto[nomeProduto], produto)
    }
  }

  criarAlertTemItensCarrinho(dados) {
    let alert = this.alertCtrl.create({
      title: 'Carrinho de compra',
      message: 'Existem produtos inseridos no seu carrinho de compra, deseja mante-los no carrinho?',
      buttons: [
        {
          text: 'Nova compra',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Sim',
          handler: () => {
            console.log('Verificar itens, se preços mudaram')
            this.carrinhoDeCompra = dados
          }
        }
      ]
    })    
    alert.present();
    
  }

  criarCard(objetos: any, produto: any) {
    var card = { 
      produto: produto, 
      quantidade: 1,
      novosObjetos: objetos,
      indices: this.indexProduto[produto.nome],
      keys: this.keysObjetos[produto.nome],
      preSelecionado: null,
      maisUmaCascata: produto.maisUmaCascata
    }
    this.card = card
    this.fecharLoading()
  }

  criarLoading (mensagem) {
    this.loading = this.load.create({
       content: mensagem ? mensagem : 'Por favor, aguarde enquanto os dados são carregados!'
    })
    this.loading.present();
  }

  getTotalCompra() {
    var total = 0
    for (let item of this.carrinhoDeCompra) {
      total += item['item'].preco * item['quantidade']
    }
    return total.toFixed(2)
  }

  getTotalItens() {
    var itens = 0
    for (let item of this.carrinhoDeCompra) {
      itens += item['quantidade']
    }
    return itens
  }

  fecharCard() {
    this.card = null
    // this.cardCarrinho = null
  }

  fecharCardCarrinho() {
    this.storage.set(`carrinhoDeCompra`, this.carrinhoDeCompra);
    this.cardCarrinho = null
  }

  fecharLoading() {
    this.loading.dismiss()
  }

  // Filtra todas as chaves, deixando apenas uma
  filtrarKeysProduto(objetosProduto: any, nomeProduto: string) {
      this.keysObjetos[nomeProduto] = new Array<Object>()
      this.keysObjetos[nomeProduto]['abrangencia'] = []
      this.keysObjetos[nomeProduto]['sabor'] = []
      this.keysObjetos[nomeProduto]['volume'] = []
      this.keysObjetos[nomeProduto]['marca'] = []
      this.keysObjetos[nomeProduto]['embalagem'] = []
      this.keysObjetos[nomeProduto]['preco'] = []
      // this.keysObjetos[nomeProduto]['subProduto'] = []

    for (let objeto of objetosProduto) {
        if(this.keysObjetos[nomeProduto]['abrangencia'].indexOf(objeto['abrangencia']) === -1) {
          this.keysObjetos[nomeProduto]['abrangencia'].push(objeto['abrangencia'])
        }

        if(this.keysObjetos[nomeProduto]['sabor'].indexOf(objeto['sabor']) === -1) {
          this.keysObjetos[nomeProduto]['sabor'].push(objeto['sabor'])
        }

        if(this.keysObjetos[nomeProduto]['volume'].indexOf(objeto['volume']) === -1) {
          this.keysObjetos[nomeProduto]['volume'].push(objeto['volume'])
        }

        if(this.keysObjetos[nomeProduto]['marca'].indexOf(objeto['marca']) === -1) {
          this.keysObjetos[nomeProduto]['marca'].push(objeto['marca'])
        }

        if(this.keysObjetos[nomeProduto]['embalagem'].indexOf(objeto['embalagem']) === -1) {
          this.keysObjetos[nomeProduto]['embalagem'].push(objeto['embalagem'])
        }

        if(this.keysObjetos[nomeProduto]['preco'].indexOf(objeto['preco']) === -1) {
          this.keysObjetos[nomeProduto]['preco'].push(objeto['preco'])
        }
    }
  }

  ordenarKeys(array) {
    for(let x = 0; x < array.length; x++)
    {
      for(let y = x + 1; y < array.length; y++ ) // sempre 1 elemento à frente
      {
        // se o (x > (x+1)) então o x passa pra frente (ordem crescente)
        if ( array[x] > array[y] )
        {
          let aux = array[x];
          array[x] = array[y];
          array[y] = aux;
        }
      }
    }
    return array
  }

  ordenarCarrinho(array) {
    for(let x = 0; x < array.length; x++)
    {
      for(let y = x + 1; y < array.length; y++ ) // sempre 1 elemento à frente
      {
        // se o (x > (x+1)) então o x passa pra frente (ordem crescente)
        if (array[x].produto > array[y].produto)
        {
          let aux = array[x];
          array[x] = array[y];
          array[y] = aux;
        }
      }
    }
    return array
  } 

  filtrarItens(atributo: string, item: any) {
    let output = new Array<Object>()
    this.card.keys = new Array<Object>()
    this.card.keys.abrangencia = [],
    this.card.keys.sabor = [],
    this.card.keys.volume = [],
    this.card.keys.marca = [],
    this.card.keys.embalagem = [],
    this.card.keys.preco = []

    for (let objeto of this.card.novosObjetos) {
      let key = objeto[atributo]
      if (key === item) {
        output.push(objeto)
          this.card.keys.abrangencia.push(objeto['abrangencia']) 
          this.card.keys.sabor.push(objeto['sabor']) 
          this.card.keys.volume.push(objeto['volume']) 
          this.card.keys.marca.push(objeto['marca']) 
          this.card.keys.embalagem.push(objeto['embalagem']) 
          this.card.keys.preco.push(objeto['preco'])
      }
    }
    this.card.novosObjetos = output
  }

  finalizaAberturaDoCard(produto: any) {
    var nomeProduto = produto.nome
    this.filtrarKeysProduto(this.objetosProduto[nomeProduto], nomeProduto)
    this.criarCard(this.objetosProduto[nomeProduto], produto)
  }

  finalizarCompra() {
     this.navCtrl.push(EntregaPage);
  }

  itemSelecionado(atributo: string, item: any) {
    this.filtrarItens(atributo, item)
    this.preSelecionarProduto()
    this.proximoSlide()

  }

  ocultaItemKey(atributo, item) {
    if (this.card.keys[atributo].indexOf(item) > -1) {
      return false
    } else {
      return true
    }
  }

  preencheObjetosProduto(data: any, produto: string){
    this.objetosProduto[produto] = data
  }

  preSelecionarProduto() {
    this.card.preSelecionado =  this.card.novosObjetos[0]
  }

  proximoSlide() {
    if (this.card.novosObjetos.length > 1) {
      this.slider.slideNext();
    } else {
      this.slider.slideTo(6);
    }    
  }

  temCard() {
    if (this.card) {
      return 'transparencia'
    }
  }

  // Carrinho
  adicionarUnidade() {
    this.card.quantidade++
  }

  colocarItemNoCarrinho() {
    if (this.isInCart()) {
      let alert = this.alertCtrl.create({
        title: 'Esse produto já está no carrinho',
        message: 'Você já inseriu ' + this.card.produto.nome + ' ' + this.card.preSelecionado.nome +
        ', deseja aumentar a quantidade desse produto no carrinho?',
        buttons: [
          {
            text: 'Não',
            role: 'cancel',
            handler: () => {
              this.card = null
            }
          },
          {
            text: 'Sim',
            handler: () => {
              for(let x = 0; x < this.carrinhoDeCompra.length; x++) {
                if (this.card.preSelecionado.id == this.carrinhoDeCompra[x].item.id) {
                  this.carrinhoDeCompra[x].quantidade += this.card.quantidade
                  this.storage.set(`carrinhoDeCompra`, this.carrinhoDeCompra);
                  this.card = null      
                  return 
                }
              }
            }
          }
        ]
      })    
      alert.present();
    } else {
      this.carrinhoDeCompra.push({item: this.card.preSelecionado, quantidade: this.card.quantidade, produto: this.card.produto.nome})
      this.storage.set(`carrinhoDeCompra`, this.carrinhoDeCompra);
      this.card = null
    }
    
  }

  isInCart() {
    // var boolean = false
    for (let item of this.carrinhoDeCompra) {
      if (this.card.preSelecionado.id == item.item.id) {
        return true
      }
    }
    return false
  }

  removerUnidade() {
    this.card.quantidade--
  }

  adicionarUmCarrinho(index) {
    this.carrinhoDeCompra[index].quantidade++
  }
 
  removerUmCarrinho(index) {
    this.carrinhoDeCompra[index].quantidade--

    if (this.carrinhoDeCompra[index].quantidade == 0) {
      this.fecharCardCarrinho()
      this.carrinhoDeCompra.splice(index, 1)
    }
  }

  mostrarMaisOpcoes(index) {
    var card = {
      index: index 
    }
    this.cardCarrinho = card
  }

  swipeLeft() {
    if(!this.card) {
      this.tabName = 'carrinho'
    }
  }

  swipeRight() {
    this.tabName = 'lista'
  }

  // Acordion da lista
  public showArea: any
  toggleDetails(area) {
    if (this.isShowArea(area)) {
      this.showArea = null 
    } else {
      this.showArea = area
    }
  }

  isShowArea(area: any) {
    return this.showArea === area
  }

}
