import { ClienteService } from './../../cliente/cliente.service';
import { PagamentoPage } from './../pagamento/pagamento';
import { CadastroEnderecoPage } from './../../cliente/cadastro-endereco/cadastro-endereco';
import { ComprarService } from './../comprar.service';
import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, AlertController, ModalController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage'

@Component({
  selector: 'page-entrega',
  templateUrl: 'entrega.html'
})
export class EntregaPage implements OnInit {
  // Loading
  protected loadingEndereco: any
  protected loadingEntrega: any

  // Ajuda no desing
  protected enderecoAlterar: boolean = false
  protected enderecos: Array<any>
  protected diasEntrega: Array<any>
  protected diaSelecionado: any

  // Informações
  protected enderecoEscolhido: any
  protected turnoEscolhido: any

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public compraService: ComprarService,
              public _clienteService: ClienteService,
              public alertCtrl: AlertController,
              public load: LoadingController,
              public storage: Storage,
              public modalCtrl: ModalController) {
    // console.log('Construtor EntregaPage')
      this.diasEntrega = new Array<Object>()
      this.enderecos = new Array<Object>()
      this.enderecoEscolhido = new Object()

    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EntregaPage');
  }

  ngOnInit() {
      let loja_id = localStorage.getItem('loja_id');
      this.getEnderecos()
      this.getTurnosEntrega(loja_id)  
  }

  criarLoading (tipo, mensagem) {
    let msg = mensagem ? mensagem : 'Por favor, aguarde enquanto as informações são carregadas!'
    if (tipo == 'endereco') {
      this.loadingEndereco = this.load.create({
         content: msg
      })
      this.loadingEndereco.present();
    } else if (tipo == 'entrega') {
      this.loadingEntrega = this.load.create({
         content: msg
      })
      this.loadingEntrega.present();
    }
  }

  fecharLoading(tipo) {
    if (tipo == 'endereco') {
      this.loadingEndereco.dismiss()
    } else if (tipo == 'entrega') {
      this.loadingEntrega.dismiss()
    }
  }

  adicionarEndereco() {
    console.log('adicionarEndereco:enderecos: ', this.enderecos)
    let profileModal = this.modalCtrl.create(CadastroEnderecoPage);
    profileModal.onDidDismiss(endereco => {
      console.log('Dentro do dismis modal: ', endereco);
      if (endereco) {
        this.enderecoEscolhido = endereco
        this.enderecos.push(endereco)
        this.verificarLojaEndereco(endereco)
        // console.log('Verificar loja se é o mesmo da compra')
      }
      this.enderecoAlterar = false
    });
    profileModal.present();
  }
  
  getEnderecos() {
    this.criarLoading('endereco', 'Por favor, aguarde enquanto localizamos seu endereço!!')
    let cliente_id = localStorage.getItem('cliente_id');
    this.compraService.getEnderecos(cliente_id).subscribe(
      data => this.tratarDadosEndereco(data),
      error => console.log('Error: ', JSON.stringify(error, null, 2))
    )
  }

  tratarDadosEndereco(data) {
    if (data.length > 0) {
      this.enderecos = data
      this.enderecoEscolhido = data[0]
    } else {
      this.enderecoAlterar = true
    }
    this.fecharLoading('endereco')
  }

  getTurnosEntrega(loja_id) {
    // let loja_id = localStorage.getItem('loja_id');
    this.criarLoading('entrega', 'Por favor!! Aguarde enquanto verificamos os dias disponíveis para entrega!!')
    this.compraService.getTurnosEntrega(loja_id).subscribe(
      data => this.tratarTurno(data),
      error => console.log('Error: ', JSON.stringify(error, null, 2))
    )
  }

  mostrarDataMascarada(dia) {
    let data = new Date(dia) 
    return data.getDate() + '/' + (data.getMonth() + 1) //+ ', ' + this.getDiaDaSemana(data.getDay())
  }

  tratarTurno(data) {
    let agoraHora = new Date().getHours()//.getHours()
    let agoraMinuto = new Date().getMinutes()

    let primeiroDia = data[0].turnos

    // console.log('Turnos: ', JSON.stringify(data, null, 2))
    for (let i = 0; i < data[0].turnos.length; i++) {
      // console.log('I: ', i)
      let turnoHora = primeiroDia[i].fechamentoDaOperacao.split(':')

      // console.log('TurnoHora: ', turnoHora)
      if (!primeiroDia[i].situacao) {
        primeiroDia.splice(i, 1)
      } else if (agoraHora > turnoHora[0]) {
          // console.log('Não da para entregar - agoraHora > turnoHora[0]')
          primeiroDia.splice(i, 1)
      } else if (agoraHora == turnoHora[0]) {
        if (agoraMinuto > turnoHora[1]) {
          // console.log('Não da para entregar - agoraMinuto > turnoHora[1]')
          primeiroDia.splice(i, 1)
        }
      }
    }

    if (primeiroDia.length == 0) {
      data.splice(0, 1)
      // console.log('Turnos Excluídos, primeiro dia excluído')
    } else {
      data[0].turnos = primeiroDia
      data.splice(4, 1)
      // console.log('Último dia excluído')
    }
    this.diasEntrega = data
    this.diaSelecionado = null
    this.turnoEscolhido = null

    this.fecharLoading('entrega')

  }

  getDiaDaSemana(dia) {
    let diaDaSemana = new Date(dia).getDay() 
    let hoje = new Date()
    if (hoje.getDay() == diaDaSemana) {
      return 'Hoje'
    } else if ((hoje.getDay() + 1) == diaDaSemana) {
      return 'Amanhã'
    } else {
      switch(diaDaSemana) {
        case 0: return 'Domingo'
        case 1: return 'Segunda'
        case 2: return 'Terça'
        case 3: return 'Quarta'
        case 4: return 'Quinta'
        case 5: return 'Sexta'
        case 6: return 'Sábado'
      }
    }
  }

  escolherFormaDePagamento() {
    if (!this.turnoEscolhido) {
      // console.log('Escolha o turno')
      let alert = this.alertCtrl.create({
        title: 'Horário não selecionado',
        subTitle: 'O horário de entrega da compra não foi selecionado, por favor, escolha o melhor dia e horário para entregarmos sua compra!!',
        buttons: ['Ok']
      })    
      alert.present();
    } else {
      let entrega = {enderecoEntrega: this.enderecoEscolhido.id, turnoEntrega: this.turnoEscolhido.id}
      this.navCtrl.push(PagamentoPage, {entrega: entrega, endereco: this.enderecoEscolhido});
    }
  }

  verificarLojaEndereco(endereco) {
    this._clienteService.getLojaIdWithAdrres(endereco).subscribe(
      data => this.tratarLoja(data[0].id),//localStorage.setItem('loja_id', data[0].id),
      error => console.log('Error: ', JSON.stringify(error, null, 2))
    )
  }

  tratarLoja(lojaEndereco) {
    let loja_id_aplicativo = localStorage.getItem('loja_id')
    if (lojaEndereco != loja_id_aplicativo) {
      console.log('Diferente - avisar')
    } else {
      console.log('Mesma coisa - avisar')
    }
    this.getTurnosEntrega(lojaEndereco)
     
  }

  selecionarEndereco(endereco) {
    console.log('selecionarEndereco(endereco) {')
    this.enderecoEscolhido = endereco
    this.verificarLojaEndereco(endereco)
    this.enderecoAlterar = false
  }

  selecionarDia(dia) {
    this.diaSelecionado = dia
     this.turnoEscolhido = null
  }

  selecionarTurno(turno) {
    this.turnoEscolhido = turno
  }

  classDiaSelecionado(dia) {
    if (dia == this.diaSelecionado) {
      return 'false'  
    } else {
      return 'true'
    }
  }

  mostrarTurno(turno) {
    if (turno.situacao) {
      return true
    } 
    return false
  }

}
