import { LocalizacaoUsuario } from './../../util/localizacao/localizacao';
import { ComprarService } from './comprar.service';
import { FinalizacaoPage } from './finalizacao/finalizacao';
import { PagamentoPage } from './pagamento/pagamento';
import { EntregaPage } from './entrega/entrega';
import { CompraPage } from './compra/compra';

import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
    ],
    exports: [],
    declarations: [
      CompraPage,
      EntregaPage,
      PagamentoPage,
      FinalizacaoPage
    ],
    providers: [ComprarService, LocalizacaoUsuario],
})
export class ComprarModule { }
