import { CadastroEnderecoPage } from './../../cliente/cadastro-endereco/cadastro-endereco';
import { ClienteService } from './../../cliente/cliente.service';
import { CompraPage } from './../compra/compra';
import { LocalizacaoUsuario } from './../../../util/localizacao/localizacao';
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ModalController } from 'ionic-angular';

/*
  Generated class for the Finalizacao page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-inicio',
  templateUrl: 'inicioPagina.html'
})
export class InicioPaginaPage {
  // Loading
  protected loading: any

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public _localizacao: LocalizacaoUsuario,
    public _clienteService: ClienteService,
    public load: LoadingController,
    public modalCtrl: ModalController) { 

    this.criarLoading(null)
     LocalizacaoUsuario.lojaParaPesquisarPreco.subscribe(
        resultado => {
          if (resultado.id > 0) {
             localStorage.setItem('loja_id', resultado.id)
             this.irAsCompras()    
          } else {
            let cliente_id = localStorage.getItem('cliente_id') 
            this._clienteService.getInformacoesCliente(cliente_id).subscribe(
              data => this.tratarDadosCliente(data),
              error => console.log('Error: ', JSON.stringify(error, null, 2))
            )
          }
      })
  }

  ionViewDidLoad() {
    console.log('InicioPaginaPage')
  }

  tratarDadosCliente(dados) {
    console.log('Dados: ', JSON.stringify(dados, null, 2))
    if (dados.enderecos.length == 0) {
      this.fecharLoading()
      this.abrirModalEndereco()
      console.log('Cadastrar endereco')
    } else {
      console.log('Procurar loja do endereço: ', dados.enderecos[0])
      this.setLoja(dados.enderecos[0])
      //this.irAsCompras()
    }
  }  

  setLoja(endereco) {
    this._clienteService.getLojaIdWithAdrres(endereco).subscribe(
      data => {
        console.log('Loja achada: ', data[0].id)
        localStorage.setItem('loja_id', data[0].id)
        this.irAsCompras()},
      error => console.log('Error: ', JSON.stringify(error, null, 2))
    )
  }

  cadastrarEndereco() {
    this.abrirModalEndereco()
  }

  abrirModalEndereco() {
    let profileModal = this.modalCtrl.create(CadastroEnderecoPage);
      profileModal.onDidDismiss(endereco => {
      console.log('Dentro do dismis modal: ', endereco);
      if (endereco) {
        this.setLoja(endereco)
        this.irAsCompras()
      }
    });
    profileModal.present();
  }

  irAsCompras() {
    this.fecharLoading()
    this.navCtrl.setRoot(CompraPage)
  }

  criarLoading (mensagem) {
    this.loading = this.load.create({
       content: mensagem ? mensagem : 'Por favor, aguarde enquanto as informações são carregadas!'
    })
    this.loading.present();
  }

  fecharLoading() {
    this.loading.dismiss()
  }
}
