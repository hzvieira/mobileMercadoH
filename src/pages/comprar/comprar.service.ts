import { HttpService } from './../../app/app.interception.service';
import { Injectable } from '@angular/core';

import { SERVER } from './../../app/app.constantes'

@Injectable()
export class ComprarService {

  constructor(private _http: HttpService) { }

  getAreas() {
      return this._http.request(SERVER.endereco + 'aplicativo/area')
      .map(res=>res.json()) 
  }

  getItens(idProduto, idLoja) {
    console.log('Get Itens: loja: ', idLoja)
      return this._http.request(SERVER.endereco + 'aplicativo/produto/itens/' + idProduto + '/' + idLoja)
      .map(res=>res.json())
  }

  getItensLojaAtualizada(idLoja, ids) {
    // console.log('getItensLojaAtualizada(idLoja, ids) { ', ids)
    return this._http.request(SERVER.endereco + 'aplicativo/produto/itens/loja/' + idLoja + '/' + ids)
      .map(res=>res.json())
  }

  getEnderecos(idCliente) {
    return this._http.request(SERVER.endereco + 'aplicativo/cliente/endereco/' + idCliente)
      .map(res=>res.json())
  }

  getTurnosEntrega(idLoja) {
    return this._http.request(SERVER.endereco + 'aplicativo/loja/turnos/' + idLoja)
      .map(res=>res.json())
  }

  getInformacoesLoja(idLoja) {
    return this._http.request(SERVER.endereco + 'aplicativo/loja/informacoes/' + idLoja)
      .map(res=>res.json())
  }

  postCompra(objeto) {
    return this._http.post(SERVER.endereco + 'aplicativo/compra/concluir/', objeto)
      .map(res=>res.json())
  }

  getComprasRealizadas(idCliente) {
    return this._http.request(SERVER.endereco + 'aplicativo/compra/comprasRealizadas/' + idCliente)
      .map(res=>res.json())
  }

}
