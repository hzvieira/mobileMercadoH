import { Http, Headers } from '@angular/http';
import { SERVER } from './../../app/app.constantes';
import { HttpService } from './../../app/app.interception.service';

import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';

@Injectable()
export class LocalizacaoService {
  constructor(private _http: HttpService, private httpExterno: Http) { }

  getEnderecoMaps(coordenadas) {
    let headers = new Headers();
    headers.append('content-type', 'application/json')
    
    let coords = coordenadas.latitude + ',' + coordenadas.longitude

    return this.httpExterno.request('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + coords + '&key=AIzaSyDkVSUJJcUjqwnwLFdn7F0oO8ENZLT2_CQ', headers) //&key=' + 'AIzaSyDkVSUJJcUjqwnwLFdn7F0oO8ENZLT2_CQ')
    //  return this.httpExterno.request('https://maps.googleapis.com/maps/api/geocode/json?latlng=-23.304082,-51.1534154', headers) //&key=' + 'AIzaSyDkVSUJJcUjqwnwLFdn7F0oO8ENZLT2_CQ')
      .map(res=>res.json())
  }

  getEnderecoMapsAddress(endereco) {
    let headers = new Headers();
    headers.append('content-type', 'application/json')

    return this.httpExterno.request('https://maps.googleapis.com/maps/api/geocode/json?address=' + endereco, headers) //&key=' + 'AIzaSyDkVSUJJcUjqwnwLFdn7F0oO8ENZLT2_CQ')
      .map(res=>res.json())
  }

  getLojaQueMaisCompra(cliente_id: any) {
    return this._http.request(SERVER.endereco + 'aplicativo/cliente/lojaQueMaisCompra/' + cliente_id)
      .map(res=>res.json())
  }

  getLojaId(endereco) {
    return this._http.post(SERVER.endereco + 'aplicativo/loja/qualLoja/', endereco)
      .map(res=>res.json())
  }

  getCidadeId(endereco) {
    console.log('Cidade: ', endereco)
    return this._http.post(SERVER.endereco + 'aplicativo/cidade/qualCidade/', endereco)
      .map(res=>res.json())
  }

  getCidades() {
    return this._http.request(SERVER.endereco + 'aplicativo/cidade/list/')
      .map(res=>res.json())
  }

}
