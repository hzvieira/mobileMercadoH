// import { Geolocation } from 'ionic-native';
import { Injectable, EventEmitter } from '@angular/core';
import { LocalizacaoService } from './localizacao.service';

@Injectable()
export class LocalizacaoUsuario {
  // private lojaQueMaisCompra
  // private lojaDoEndereco
  // private enderecoUsuario: any

  static lojaParaPesquisarPreco = new EventEmitter<any>();

  constructor(private _localizacaoService: LocalizacaoService) {
    this.getCidadeQueMaisCompra()
    // this.getPosicao()
  }

  getCidadeQueMaisCompra() {
    // console.log('LocalizacaoUsuario')
    let cliente_id = localStorage.getItem('cliente_id');
    this._localizacaoService.getLojaQueMaisCompra(cliente_id).subscribe(
      data => this.tratarDadosCidadeQueMaisCompra(data),
      error => console.log('Error: ', JSON.stringify(error, null, 2)),
    )
  }

  tratarDadosCidadeQueMaisCompra(data: any) {
    if (data.rowCount == 0) {
      // this.getPosicao()
      LocalizacaoUsuario.lojaParaPesquisarPreco.emit({id: 0})
    } else {
      this.filtrarCidadeDados(data.rows)
    }
  }

  filtrarCidadeDados(linhas) {
    LocalizacaoUsuario.lojaParaPesquisarPreco.emit({id: linhas[0].loja_id})
    // localStorage.setItem('loja_id', linhas[0].loja_id)
  }

  // getPosicao() {
  //   Geolocation.getCurrentPosition().then(position => {
  //     this.getDadosPosition(position.coords)
  //   }, (err) => {
  //     console.log(err);
  //   });
  // }

  // getDadosPosition(coords) {
  //   this._localizacaoService.getEnderecoMaps(coords).subscribe(
  //     data => this.filtrarDadosMaps(data),
  //     error => console.log('Error: ', error)
  //   )
  // }

  // filtrarDadosMaps(data: any) {
  //   this.enderecoUsuario = new Object()

  //   let dados = data.results[0].address_components
  //   let cidade: any = new Object()
    

  //   dados.forEach(element => {
  //     element.types.forEach(tipo => {
  //       if (tipo == 'street_number') {
  //         this.enderecoUsuario.numero = element.long_name
  //       }
  //       if (tipo == 'route') {
  //         this.enderecoUsuario.rua = element.long_name
  //       }
  //       if (tipo == 'sublocality') {
  //         this.enderecoUsuario.bairro = element.long_name
  //       }
  //       if (tipo == 'administrative_area_level_2') {
  //         cidade.cidade = element.long_name
  //       }
  //       if (tipo == 'administrative_area_level_1') {
  //         cidade.estado = element.short_name
  //       }
  //       if (tipo == 'postal_code') {
  //         this.enderecoUsuario.cep = element.short_name
  //       }
  //     });
  //   });
  //   this.getLojaCidade(cidade) 
  // }

  // getLojaCidade(endereco) {
  //   //console.log('Endereço: ', endereco)
  //   endereco =  {cidade: "Maringá", estado: "PR"}
  //   this._localizacaoService.getLojaId(endereco).subscribe(
  //     data => this.atribuirLojaCidade(data, endereco),//LocalizacaoUsuario.lojaParaPesquisarPreco.emit(data),
  //     error => console.log('Error deu bom não: ', error),
  //   )
  // }

  // atribuirLojaCidade(data, endereco) {
  //   if (data.length == 0) {
  //     // localStorage.setItem('loja_id', null)
  //     LocalizacaoUsuario.lojaParaPesquisarPreco.emit({id: 0, endereco: endereco})  
  //   } else {
  //     let loja_id = data[0].id
  //     // localStorage.setItem('loja_id', loja_id)
  //     LocalizacaoUsuario.lojaParaPesquisarPreco.emit({id: loja_id})
  //   } 
  // }    
}





/* 
import { Geolocation } from 'ionic-native';
import { Injectable, EventEmitter } from '@angular/core';
import { LocalizacaoService } from './localizacao.service';

@Injectable()
export class LocalizacaoUsuario {

  static lojaParaPesquisarPreco = new EventEmitter<any>();
  
  constructor(private _localizacaoService: LocalizacaoService) {
    console.log('Construtor Localizacao')
    this.getCidadeQueMaisCompra()
  }

  getCidadeQueMaisCompra() {
    let cliente_id = localStorage.getItem('cliente_id');
    this._localizacaoService.getCidadeQueMaisCompra(cliente_id).subscribe(
      data => this.tratarDadosCidadeQueMaisCompra(data),
      error => console.log('Error: ', JSON.stringify(error, null, 2)),
      () => console.log('In the end ')
    )
  }  

  tratarDadosCidadeQueMaisCompra(data: any) {
    if (data.rowCount == 0) {
      console.log('Não tem cidade')
      this.getPosicao()
    } else {
      this.filtrarCidadeDados(data.rows)
    }
  }

  filtrarCidadeDados(linhas) {
    console.log('TRATAR ISSO DEPOIS COM MAIS INFORMAÇÔES - TRAZER O ID TAMBÉM')
    console.log('Linhas: ', linhas)
    LocalizacaoUsuario.lojaParaPesquisarPreco.emit('Jurerê')
    // this.lojaParaPesquisarPreco = 'Jurerê'
  }

  getPosicao() {
    Geolocation.getCurrentPosition().then(position => {
      this.getDadosPosition(position.coords)
    }, (err) => {
      console.log(err);
    });
    
  }

  getDadosPosition(coords) {
    this._localizacaoService.getEnderecoMaps(coords).subscribe(
      data => this.filtrarDadosMaps(data),
      error => console.log('Error: ', error)
      //, () => console.log('In the end getEndereco(latitude, longitude) { ')
    )
  }

  filtrarDadosMaps(data: any) {
    let breakFor = false
    let dados = data.results[0].address_components
    let endereco: any = new Object()
    
    dados.forEach(element => {
      element.types.forEach(tipo => {
        if (tipo == 'administrative_area_level_2') {
          endereco.cidade = element.long_name
        }
        if (tipo == 'administrative_area_level_1') {
          endereco.estado = element.short_name
          breakFor = true
          // LocalizacaoUsuario.lojaParaPesquisarPreco.emit(endereco) 
        }
      })
      if (breakFor) {
        this.getIdCidade(endereco)
        return
      }
    }); 
  }

  getIdCidade(endereco) {
    //console.log('Endereço: ', endereco)
     endereco =  {cidade: "Londrina", estado: "PR"}
    this._localizacaoService.getLojaId(endereco).subscribe(
      data => localStorage.setItem('loja_id', data[0].id),//LocalizacaoUsuario.lojaParaPesquisarPreco.emit(data),
      error => console.log('Error deu bom não: ', error),
      () => console.log('In the end getEndereco(latitude, longitude) { ')
    )
  }

}
*/