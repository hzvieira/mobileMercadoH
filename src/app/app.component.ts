import { ClienteService } from './../pages/cliente/cliente.service';
import { InicioPaginaPage } from './../pages/comprar/inicioPagina/inicioPagina';
import { HttpService } from './app.interception.service';
import { LoginPage } from './../pages/login/login';
import { SairPage } from './../pages/sair/sair';
import { SettingsPage } from './../pages/settings/settings';
import { ComprasRealizadasPage } from './../pages/compras-realizadas/compras-realizadas';
import { CompraPage } from './../pages/comprar/compra/compra';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, ModalController } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { SobrePage } from "../pages/sobre/sobre/sobre";
// import { Geolocation } from 'ionic-native';

@Component({
  templateUrl: 'app.html'
})
export class MercadoH {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = InicioPaginaPage;

  pages: Array<{title: string, component: any, icone: string}>;
  position: any = new Object()

  constructor(public platform: Platform, public _clienteService: ClienteService,
  public modalCtrl: ModalController) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Comprar', component: CompraPage, icone: 'cart' },
      { title: 'Compras realizadas', component: ComprasRealizadasPage, icone: 'ios-archive' },
      { title: 'Cadastro', component: SettingsPage, icone: 'settings' },
      { title: 'Sobre', component: SobrePage, icone: 'ios-information-circle-outline' },
      { title: 'Sair', component: SairPage, icone: 'log-out' }
    ];
  }

  ngAfterViewInit() {
    // this._clienteService.isLogado().subscribe(
    //   data => this.nav.setRoot(InicioPaginaPage),//console.log('Logado: ', data),//LocalizacaoUsuario.lojaParaPesquisarPreco.emit(data),
    //   error => console.log('Error: Não esta logado', error)
    // )

   HttpService.naoAutorizado.subscribe(
     resultado => this.irParaLogin() 
   )  
  }

  irParaLogin() {
    console.log('ir para login')
    let profileModal = this.modalCtrl.create(LoginPage);
    profileModal.onDidDismiss(() => {
      // window.location.reload()
      
    });
     profileModal.present()
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
