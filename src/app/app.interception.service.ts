import { Injectable, EventEmitter} from '@angular/core';
import {Http, Request, RequestOptionsArgs, Response, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class HttpService { //extends Http {
  
  static naoAutorizado = new EventEmitter<boolean>();

  constructor (private _http: Http) { }

  request(url: string|Request, options?: RequestOptionsArgs): Observable<Response> {
    let token = localStorage.getItem('token');
    if (typeof url === 'string') { // meaning we have to add the token to the options, not in url
      if (!options) {
        // let's make option object
        options = {headers: new Headers()};
      }
      options.headers.set('ocupacao', 'aplicativo');
      options.headers.set('token', token);
    } else {
    // we have to add the token to the url object
      url.headers.set('ocupacao', 'aplicativo');
      url.headers.set('token', token);
    }
    return this._http.request(url, options).catch(this.catchAuthError(this));
  }

  private catchAuthError (self: HttpService) {
    // we have to pass HttpService's own instance here as `self`
    return (res: Response) => {
      if (res.status === 401 || res.status === 403) {
        // if not authenticated
        console.log('Não Autorizado: ', res);
        HttpService.naoAutorizado.emit(true)
      }
      return Observable.throw(res);
    };
  }

  post(url: string, body): Observable<Response> {
    let token = localStorage.getItem('token');
    let options: any = {headers: new Headers()};

    options.headers.set('ocupacao', 'aplicativo');
    options.headers.set('token', token);
    
    return this._http.post(url, body, options).catch(this.catchAuthError(this));
  }

  put(url: string, body): Observable<Response> {
    let token = localStorage.getItem('token');
    let options: any = {headers: new Headers()};

    options.headers.set('ocupacao', 'aplicativo');
    options.headers.set('token', token);
    
    return this._http.put(url, body, options).catch(this.catchAuthError(this));
  }

}