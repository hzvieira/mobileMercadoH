import { PopoverCarrinho } from './../pages/comprar/pagamento/popoverCarrinho/popover';
import { ClienteDadosPessoaisPage } from './../pages/cliente/dados-pessoais/cliente-dados-pessoais';
import { ClientePrivacidadePage } from './../pages/cliente/privacidade/cliente-privacidade';
import { InicioPaginaPage } from './../pages/comprar/inicioPagina/inicioPagina';
import { FinalizacaoPage } from './../pages/comprar/finalizacao/finalizacao';
import { ClienteService } from './../pages/cliente/cliente.service';
import { CadastroEnderecoPage } from './../pages/cliente/cadastro-endereco/cadastro-endereco';
import { CadastroClientePage } from './../pages/cliente/cadastro-cliente/cadastro-cliente';
import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { Storage } from '@ionic/storage'

import { MercadoH } from './app.component';

// Pages
import { SairPage } from './../pages/sair/sair';
import { SettingsPage } from './../pages/settings/settings';
import { LoginPage } from './../pages/login/login';
import { ComprasRealizadasPage } from './../pages/compras-realizadas/compras-realizadas';
import { PagamentoPage } from './../pages/comprar/pagamento/pagamento';
import { EntregaPage } from './../pages/comprar/entrega/entrega';
import { CompraPage } from './../pages/comprar/compra/compra';
import { SobrePage } from "../pages/sobre/sobre/sobre";
import { TermoDeUsoPage } from "../pages/sobre/termo-de-uso/termo-de-uso";

// Services
import { LocalizacaoService } from './../util/localizacao/localizacao.service';
import { LoginService } from './../pages/login/login.service';
import { HttpService } from './app.interception.service';
import { ComprarService } from './../pages/comprar/comprar.service';
import { LocalizacaoUsuario } from './../util/localizacao/localizacao';
import { PoliticaPrivacidadePage } from "../pages/sobre/politica-de-privacidade/politica-privacidade";

@NgModule({
  declarations: [
    MercadoH,
    InicioPaginaPage,
    CompraPage,
    EntregaPage,
    PagamentoPage,
    PopoverCarrinho,
    FinalizacaoPage,
    ComprasRealizadasPage,
    LoginPage,
    SettingsPage,
    SobrePage,
    TermoDeUsoPage,
    PoliticaPrivacidadePage,
    CadastroClientePage,
    CadastroEnderecoPage,
    ClientePrivacidadePage,
    ClienteDadosPessoaisPage,
    SairPage
  ],
  imports: [
    IonicModule.forRoot(MercadoH)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MercadoH,
    InicioPaginaPage,
    CompraPage,
    EntregaPage,
    PagamentoPage,
    PopoverCarrinho,
    FinalizacaoPage,
    ComprasRealizadasPage,
    LoginPage,
    SettingsPage,
    SobrePage,
    TermoDeUsoPage,
    PoliticaPrivacidadePage,
    CadastroClientePage,
    CadastroEnderecoPage,
    ClientePrivacidadePage,
    ClienteDadosPessoaisPage,
    SairPage
  ],
  providers: [
    Storage,
    HttpService, 
    LoginService,
    LocalizacaoService,
    LocalizacaoUsuario,
    ComprarService,
    ClienteService

  ]
})

export class AppModule {}
